FROM ruby:2.6.3

RUN apt-get update && \
apt-get install -y nodejs postgresql-client && \
gem install bundler

WORKDIR /app

COPY Gemfile /app/Gemfile
COPY Gemfile.lock /app/Gemfile.lock

RUN bundle install

COPY . /app

CMD ["rails", "server", "-b", "0.0.0.0"]
